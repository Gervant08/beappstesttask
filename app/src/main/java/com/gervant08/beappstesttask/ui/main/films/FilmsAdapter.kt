package com.gervant08.beappstesttask.ui.main.films

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gervant08.beappstesttask.R
import com.gervant08.beappstesttask.model.api.FilmsApi
import com.gervant08.beappstesttask.model.data.pojo.Film
import com.gervant08.beappstesttask.ui.common.RequestListener

class FilmsAdapter(
    private val likeListener: (Film) -> Unit,
    private val filmSelectedListener: (Film) -> Unit
): ListAdapter<Film, FilmsAdapter.FilmsViewHolder>(FilmsItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmsViewHolder =
        FilmsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_film, parent, false))


    override fun onBindViewHolder(holder: FilmsViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener{ filmSelectedListener(getItem(position)) }
    }


    inner class FilmsViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        private val filmImage: ImageView = view.findViewById(R.id.ivFilmImage)
        private val likeFilmButton: ImageView = view.findViewById(R.id.ibLikeFilm_detailed)
        private val filmTitle: TextView = view.findViewById(R.id.tvFilmTitle)
        private val downloadProgress: ProgressBar = view.findViewById(R.id.pbDownloadProgress)

        fun bind(item: Film) {
            Glide.with(view)
                .load(FilmsApi.getImageUrl(item.cover!!.id!!))
                .listener(RequestListener.getListener(downloadProgress))
                .centerCrop()
                .into(filmImage)

            filmTitle.text = item.title
            likeFilmButton.setOnClickListener{ whenLikeButtonPressed(item) }
            checkItemOnLike(item)

        }

        private fun checkItemOnLike(item: Film){
            if (item.isLiked){
                likeFilmButton.setImageResource(R.drawable.ic_like_enabled)
            } else{
                likeFilmButton.setImageResource(R.drawable.ic_like_disabled)
            }

        }

        private fun whenLikeButtonPressed(film: Film){
            if (film.isLiked){
                film.isLiked = false
                likeFilmButton.setImageResource(R.drawable.ic_like_disabled)
                likeListener(film)
            } else{
                film.isLiked = true
                likeFilmButton.setImageResource(R.drawable.ic_like_enabled)
                likeListener(film)
            }

        }
    }
}