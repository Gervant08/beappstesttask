package com.gervant08.beappstesttask.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gervant08.beappstesttask.model.data.pojo.Genre
import com.gervant08.beappstesttask.R
import com.gervant08.beappstesttask.model.data.pojo.Film
import com.gervant08.beappstesttask.ui.main.films.FilmsAdapter

class MainAdapter(
    private val likeListener: (Film) -> Unit,
    private val filmChooseListener: (Film) -> Unit
) : ListAdapter<Genre, MainAdapter.MainViewHolder>(GenreItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_films, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class MainViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val tvCategoryTitle: TextView = view.findViewById(R.id.tvCategoryTitle)
        private val rvFilms: RecyclerView = view.findViewById(R.id.rvFilms)


        fun bind(item: Genre) {
            tvCategoryTitle.text = item.title
            val filmsAdapter = FilmsAdapter (
                { film -> likeListener(film) },
                { film -> filmChooseListener(film)})

            rvFilms.adapter = filmsAdapter
            filmsAdapter.submitList(item.content)
        }
    }

}