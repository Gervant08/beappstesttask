package com.gervant08.beappstesttask.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.gervant08.beappstesttask.R
import com.gervant08.beappstesttask.model.data.FilmLiveData
import com.gervant08.beappstesttask.model.data.pojo.Film
import com.gervant08.beappstesttask.model.data.pojo.Genre
import com.gervant08.beappstesttask.model.database.entities.EntityFilm
import com.gervant08.beappstesttask.ui.common.FragmentsNames
import com.gervant08.beappstesttask.ui.detailed.FilmDetailedFragment

class MainFragment: Fragment(R.layout.fragment_main) {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var recyclerView: RecyclerView
    private val adapter = MainAdapter(
        {film -> whenLikeButtonPressed(film) },
        {film -> chooseFilm(film) })


    private fun chooseFilm(film: Film) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, FilmDetailedFragment.newInstance(film))
            .addToBackStack(FragmentsNames.FRAGMENT_MAIN)
            .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FilmLiveData.genresList.observe(this, this::listHasChanged)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.rv_main)
        recyclerView.adapter = adapter
        viewModel.loadData()
    }

    private fun whenLikeButtonPressed(film: Film) {
        val newFilm = EntityFilm(film.id!!, film.title!!, film.isLiked)
        if (film.isLiked){
            viewModel.saveFilmsToDatabase(newFilm)
        }else{
            viewModel.deleteFilmFromDatabase(newFilm)
        }

    }

    private fun listHasChanged(genresList: List<Genre>) {
        adapter.submitList(viewModel.checkDatabase(genresList))
    }

}