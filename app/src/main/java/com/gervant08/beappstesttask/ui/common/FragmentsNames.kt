package com.gervant08.beappstesttask.ui.common

object FragmentsNames {
    const val FRAGMENT_MAIN = "MainFragment"
    const val FRAGMENT_DETAILED = "DetailedFragment"
}