package com.gervant08.beappstesttask.ui.detailed

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.gervant08.beappstesttask.R
import com.gervant08.beappstesttask.model.api.FilmsApi
import com.gervant08.beappstesttask.model.data.pojo.Film
import com.gervant08.beappstesttask.ui.common.FragmentsNames
import com.gervant08.beappstesttask.ui.common.RequestListener
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

class FilmDetailedFragment : Fragment(R.layout.fragment_film_detailed), Serializable {

    private lateinit var coverImageView: ImageView
    private lateinit var titleTextView: TextView
    private lateinit var createDateTextView: TextView
    private lateinit var languagesTextView: TextView
    private lateinit var downloadProgressBar: ProgressBar
    private lateinit var filmAvailableLanguages: String
    private lateinit var filmCreationDate: String
    private lateinit var selectedFilm: Film
    private val viewModel: FilmDetailedViewModel by viewModels()

    companion object {
        private const val ARG_FILM = "Film"
        const val REQUIRED_DATE_PATTERN = "dd.MM.yy"
        const val CURRENT_DATE_PATTERN = "yyyy.MM.dd"

        fun newInstance(film: Film): FilmDetailedFragment {
            val args = Bundle().apply {
                putSerializable(ARG_FILM, film)
            }
            return FilmDetailedFragment().apply {
                arguments = args
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedFilm = arguments?.getSerializable(ARG_FILM) as Film
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        coverImageView = view.findViewById(R.id.ivCover)
        titleTextView = view.findViewById(R.id.tvTitle)
        createDateTextView = view.findViewById(R.id.tvCreateDate)
        languagesTextView = view.findViewById(R.id.tvLanguages)
        downloadProgressBar = view.findViewById(R.id.pbDetailedDownloadProgress)
        getStrings()
        setUpViews()
    }

    private fun getStrings() {
        filmAvailableLanguages = getString(R.string.film_available_languages)
        filmCreationDate = getString(R.string.film_date_of_creation)
    }

    private fun setUpViews() {
        Glide.with(requireContext())
            .load(FilmsApi.getImageUrl(selectedFilm.cover!!.id!!))
            .listener(RequestListener.getListener(downloadProgressBar))
            .centerCrop()
            .into(coverImageView)

        titleTextView.text = selectedFilm.title
        createDateTextView.text = dateTransformation(selectedFilm.createdAt!!)
        languagesTextView.text = languagesTransformation()
    }

    //Converting the received date to the required date
    private fun dateTransformation(createDate: String): String {
        val date = SimpleDateFormat(CURRENT_DATE_PATTERN, Locale.US).parse(createDate.substringBefore(" ").replace("-", "."))
        return ("$filmCreationDate ${SimpleDateFormat(REQUIRED_DATE_PATTERN, Locale.US).format(date!!)}")

    }

    //Converting the list of languages to a string
    private fun languagesTransformation(): String {
        var languages = ""
        selectedFilm.languages?.forEach { languages += "${it?.title}. " }
        return ("$filmAvailableLanguages $languages")
    }

}