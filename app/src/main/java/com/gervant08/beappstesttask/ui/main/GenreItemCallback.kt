package com.gervant08.beappstesttask.ui.main

import androidx.recyclerview.widget.DiffUtil
import com.gervant08.beappstesttask.model.data.pojo.Genre

class GenreItemCallback: DiffUtil.ItemCallback<Genre>() {
    override fun areItemsTheSame(oldItem: Genre, newItem: Genre): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Genre, newItem: Genre): Boolean {
        return oldItem == newItem
    }

}