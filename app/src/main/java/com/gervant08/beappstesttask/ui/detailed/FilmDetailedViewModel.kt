package com.gervant08.beappstesttask.ui.detailed

import androidx.lifecycle.ViewModel
import com.gervant08.beappstesttask.model.data.pojo.Film

class FilmDetailedViewModel: ViewModel() {
    private  var selectedFilm: Film? = null

    fun saveSelectedFilm(selectedFilm: Film) {
        this.selectedFilm = selectedFilm
    }

    fun getSelectedFilm() = selectedFilm
}