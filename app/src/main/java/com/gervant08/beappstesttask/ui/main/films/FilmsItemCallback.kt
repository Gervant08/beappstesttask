package com.gervant08.beappstesttask.ui.main.films

import androidx.recyclerview.widget.DiffUtil
import com.gervant08.beappstesttask.model.data.pojo.Film

class FilmsItemCallback: DiffUtil.ItemCallback<Film>() {
    override fun areItemsTheSame(oldItem: Film, newItem: Film): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Film, newItem: Film): Boolean {
        return oldItem == newItem
    }

}