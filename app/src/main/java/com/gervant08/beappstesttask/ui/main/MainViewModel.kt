package com.gervant08.beappstesttask.ui.main

import androidx.lifecycle.ViewModel
import com.gervant08.beappstesttask.model.api.FilmsApi
import com.gervant08.beappstesttask.model.data.pojo.Genre
import com.gervant08.beappstesttask.model.database.FilmDatabase
import com.gervant08.beappstesttask.model.database.entities.EntityFilm

class MainViewModel : ViewModel() {
    private val database = FilmDatabase.instance

    fun loadData() {
        FilmsApi.getFilmsGenres()
    }

    fun saveFilmsToDatabase(newEntityFilm: EntityFilm) {
        readFilmsFromDatabase().forEach { entityFilm ->
            if (entityFilm.title == newEntityFilm.title) {
                return
            }
        }
        database.filmDao().insert(newEntityFilm)
    }

    private fun readFilmsFromDatabase(): List<EntityFilm> {
        return database.filmDao().getAll()
    }

    fun deleteFilmFromDatabase(entityFilm: EntityFilm) {
        database.filmDao().deleteById(entityFilm.id)
    }

    //Replaces movies with favorites, if they are in the database
    //If the database is empty, method returns an unmodified list
    fun checkDatabase(genresList: List<Genre>): List<Genre> {
        genresList.forEach { genre ->
            genre.content?.forEach { oldFilm ->
                readFilmsFromDatabase().forEach { newFilm ->
                    if (oldFilm.title == newFilm.title && !oldFilm.isLiked) {
                        oldFilm.isLiked = true
                    }
                }
            }
        }
        return genresList
    }

}