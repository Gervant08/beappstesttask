package com.gervant08.beappstesttask.ui.root

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import com.gervant08.beappstesttask.R
import com.gervant08.beappstesttask.ui.detailed.FilmDetailedFragment
import com.gervant08.beappstesttask.ui.main.MainFragment

class RootActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)

        if (savedInstanceState == null){
            supportFragmentManager.beginTransaction()
                .add(R.id.fragmentContainer, MainFragment())
                .add(R.id.fragmentContainer, FilmDetailedFragment())
                .replace(R.id.fragmentContainer, MainFragment())
                .commit()
        }


    }
}