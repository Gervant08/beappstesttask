package com.gervant08.beappstesttask.model.api

import com.gervant08.beappstesttask.model.data.pojo.Response
import retrofit2.Call
import retrofit2.http.GET

interface FilmsApiService {

    @GET("api/main_page")
    fun getFilms(): Call<Response>
}