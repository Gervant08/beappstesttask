package com.gervant08.beappstesttask.model.api

import com.gervant08.beappstesttask.model.api.Common.BASE_URL_GET_GENRES
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    private var retrofit: Retrofit? = null

    private fun getClient(client: OkHttpClient): Retrofit {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL_GET_GENRES)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit!!
    }

    fun getRetrofitService(client: OkHttpClient): FilmsApiService =
        getClient(client).create(FilmsApiService::class.java)
}