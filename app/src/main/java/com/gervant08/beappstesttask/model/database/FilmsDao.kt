package com.gervant08.beappstesttask.model.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.gervant08.beappstesttask.model.database.entities.EntityFilm

@Dao
interface FilmsDao {
    @Query("SELECT * FROM films")
    fun getAll(): List<EntityFilm>

    @Insert
    fun insertAll(entityFilms: List<EntityFilm>)

    @Insert
    fun insert(entityFilm: EntityFilm): Long

    @Update
    fun update(entityFilm: EntityFilm)

    @Query("DELETE FROM films WHERE id = :filmID")
    fun deleteById(filmID: Int)
}