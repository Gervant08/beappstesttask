package com.gervant08.beappstesttask.model.data.pojo

data class Genre(
    val id: Int? = null,
    val title: String? = null,
    var content: ArrayList<Film>? = null,
)