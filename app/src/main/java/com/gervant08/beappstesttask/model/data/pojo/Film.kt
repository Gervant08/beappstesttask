package com.gervant08.beappstesttask.model.data.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Film(
    val id: Int? = null,
    val title: String? = null,
    val cover: Cover? = null,
    val genreId: Int? = null,
    val languages: List<LanguagesItem?>? = null,
    @SerializedName("created_at")
	val createdAt: String? = null,
    var isLiked: Boolean = false
): Serializable