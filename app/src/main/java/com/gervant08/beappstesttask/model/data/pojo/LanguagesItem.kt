package com.gervant08.beappstesttask.model.data.pojo

data class LanguagesItem(
	val title: String? = null
)