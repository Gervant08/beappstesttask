package com.gervant08.beappstesttask.model.data.pojo

import com.google.gson.annotations.SerializedName

data class Response(
	@SerializedName("content")
	val genreList: ArrayList<Genre>? = null
)



