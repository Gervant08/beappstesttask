package com.gervant08.beappstesttask.model.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "films")
data class EntityFilm (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String,
    var isLiked: Boolean = false
)