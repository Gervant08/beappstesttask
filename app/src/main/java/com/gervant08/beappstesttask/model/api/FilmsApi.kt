package com.gervant08.beappstesttask.model.api

import com.gervant08.beappstesttask.model.data.FilmLiveData
import com.gervant08.beappstesttask.model.data.pojo.Response
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback

object FilmsApi {
    private val client = OkHttpClient.Builder().build()
    private val service = RetrofitClient.getRetrofitService(client)

    fun getFilmsGenres() {
        service.getFilms().enqueue(object : Callback<Response>{
            override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                if (response.isSuccessful){
                    FilmLiveData.genresList.value = response.body()?.genreList
                }
            }

            override fun onFailure(call: Call<Response>, t: Throwable) {

            }

        })
    }
    fun getImageUrl(imageId: String): String = ("${Common.BASE_URL_DOWNLOAD_IMAGE}$imageId.jpg")
}