package com.gervant08.beappstesttask.model.data

import androidx.lifecycle.MutableLiveData
import com.gervant08.beappstesttask.model.data.pojo.Genre

object FilmLiveData {
    val genresList: MutableLiveData<ArrayList<Genre>> = MutableLiveData()
}