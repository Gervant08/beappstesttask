package com.gervant08.beappstesttask.model.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.gervant08.beappstesttask.app.App
import com.gervant08.beappstesttask.model.database.entities.EntityFilm

@Database(entities = [EntityFilm::class], version = 1)
abstract class FilmDatabase: RoomDatabase() {

    abstract fun filmDao(): FilmsDao

    companion object{
        private const val DATABASE_NAME = "Films.db"

        val instance: FilmDatabase by lazy {
            Room.databaseBuilder(
                App.applicationContext,
                FilmDatabase::class.java,
                DATABASE_NAME)
                .allowMainThreadQueries()
                .build()
        }
    }

}